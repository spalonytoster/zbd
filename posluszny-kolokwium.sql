--ZADANIA NA KOLOKWIUM

--Imię: Maciej
--Nazwisko: Posłuszny
--Semestr: I
--Specjalność: -


--UZUPEŁNIJ ROZWIĄZANIA ZADAŃ I TESTY POPRAWNOŚCI.



--K1. Wylosuj 6 różnych liczb całkowitych z przedziału od 1 do 49.
--Użyj kolekcji. Wypisz wylosowane liczby na ekranie.
--Rozwiązanie umieść w procedurze.
--Zademonstruj działanie zaimplementowanej procedury.
--Rozwiązanie:

set serveroutput on;

declare
  procedure wylosuj IS
    v_kolekcja INT;
  begin
    for v_i in 1..6 LOOP
      dbms_output.put_line(round(dbms_random.value(1, 49)));
    end loop;
  end;

--Test poprawności rozwiązania:
begin
  wylosuj();
end;
/


--K2. W dowolnej funkcji obsłuż wyjątek: NO_DATA_FOUND.
--Nie ma znaczenia co robi dana funkcji.
--Zademonstruj działanie zaimplementowanej funkcji.
--Rozwiązanie:
SET SERVEROUTPUT ON;

DROP TABLE pracownik;

CREATE TABLE pracownik (
  id_pracownik   INT,
  imie           VARCHAR2(15) NOT NULL,
  nazwisko       VARCHAR2(20) NOT NULL
);

INSERT INTO pracownik (
  id_pracownik,
  imie,
  nazwisko
) VALUES (
  1,
  'Jan',
  'Kowalski'
);

INSERT INTO pracownik (
  id_pracownik,
  imie,
  nazwisko
) VALUES (
  2,
  'Jerzy',
  'Nowak'
);

DECLARE
  v_imie_pracownika   pracownik.imie%TYPE;

  FUNCTION znajdz_pracownika (
    p_id INT
  ) RETURN VARCHAR2 IS
    v_imie   pracownik.imie%TYPE;
  BEGIN
    SELECT imie INTO
      v_imie
    FROM pracownik
    WHERE id_pracownik = p_id;

    dbms_output.put_line(v_imie);

    RETURN v_imie;

    EXCEPTION
      WHEN no_data_found THEN
        return 'BLAD';
      WHEN OTHERS THEN
        return 'BLAD';
  END;

  BEGIN
  --Test poprawności rozwiązania nie prowokujący wyjątku:
  v_imie_pracownika := znajdz_pracownika(1);

  --Test poprawności rozwiązania prowokujący wyjątek:
  v_imie_pracownika := znajdz_pracownika(100);
END;


--K3. Napisać procedurę wypisującą n pierwszych wyrazów ciągu Fibbonacciego,
--gdzie n jest parametrem procedury.
--Wzór: f(0)=0, f(1)=1, f( n )=f(n-1)+f(n-2).
--Zademonstruj działanie zaimplementowanej procedury.
--Rozwiązanie:
set serveroutput on;

DECLARE
  v_i INT;

  function fib(n INT) return int
  is
  begin
    if n = 1 then
        return 1;
    elsif n = 2 then
      return 2;
    else
      return fib(n-1) + fib(n-2);
    end if;
  end;

  procedure print_fib (n INT)
  is
  begin
    if n < 2 then
      dbms_output.put_line(fib(n));
    end if;

    for v_i in 1..n loop
      DBMS_OUTPUT.PUT_LINE(fib(v_i));
    end loop;
  end;


BEGIN
  --Test poprawności rozwiązania:
  print_fib(7);
END;
/



--K4. Rozwiązania zadań K1-K3 umieść w pakiecie i zademonstruj, że
--działają wszystkie funkcje i procedury w utworzonym pakiecie.
--Rozwiązanie:
set serveroutput on;

create or replace package kolokwium is
  v_i INT;

  procedure wylosuj;

  FUNCTION znajdz_pracownika (p_id INT) RETURN VARCHAR2;

  function fib(n int) return int;
  procedure print_fib(n int);

end kolokwium;
/

create or replace package body kolokwium is
--  zad 1
  procedure wylosuj IS
    v_kolekcja INT;
  begin
    for v_i in 1..6 LOOP
      dbms_output.put_line(round(dbms_random.value(1, 49)));
    end loop;
  end;

--  zad 2
  FUNCTION znajdz_pracownika (
    p_id INT
  ) RETURN VARCHAR2 IS
    v_imie   pracownik.imie%TYPE;
  BEGIN
    SELECT imie INTO
      v_imie
    FROM pracownik
    WHERE id_pracownik = p_id;

    dbms_output.put_line(v_imie);

    RETURN v_imie;

    EXCEPTION
      WHEN no_data_found THEN
        return 'BLAD';
      WHEN OTHERS THEN
        return 'BLAD';
  END;

--  zad 3
  function fib(n INT) return int
  is
  begin
    if n = 1 then
        return 1;
    elsif n = 2 then
      return 2;
    else
      return fib(n-1) + fib(n-2);
    end if;
  end;

  procedure print_fib (n INT)
  is
  begin
    if n < 2 then
      dbms_output.put_line(fib(n));
    end if;

    for v_i in 1..n loop
      DBMS_OUTPUT.PUT_LINE(fib(v_i));
    end loop;
  end;
end kolokwium;
/

--Test poprawności rozwiązania:
DECLARE
  v_imie_pracownika   pracownik.imie%TYPE;
BEGIN
--  zad 1
  dbms_output.put_line('------------Zad1----------');
  kolokwium.wylosuj;

--  zad 2
  dbms_output.put_line('----------Zad2------------');
  v_imie_pracownika := kolokwium.znajdz_pracownika(1);
  v_imie_pracownika := kolokwium.znajdz_pracownika(100);

--  zad 3
  dbms_output.put_line('---------Zad3-------------');
  kolokwium.print_fib(7);
END;
/

--K5. Dana jest tabela imiona(id,imie) oraz tabela imiona_usuniete(id,imie,data).
--Napisać wyzwalacz, który usunięte imiona z tabeli imiona zapisze w tabeli
--imiona_usuniete razem z datą usunięcia:
--Rozwiązanie
--Etap 1: Tworzymy tabele i dodajemy dane:
drop table imiona;
create table imiona(imie_id int, imie varchar2(20));
insert into imiona values (1, 'Maciej');
insert into imiona values (2, 'Błażej');
insert into imiona values (3, 'Karol');

drop table imiona_usuniete;
create table imiona_usuniete(imie_id int, imie varchar2(20), data_usuniecia date);

--Etap 2: Tworzymy wyzwalacz:

create or replace trigger trg_imiona
before delete on imiona
for each row
begin
  insert into imiona_usuniete values (:old.imie_id, :old.imie, sysdate);
end;
/

--Etap 3: Testujemy poprawność wyzwalacza:
begin
  delete from imiona
  where imie_id=1;
end;
/

select * from imiona;
select * from imiona_usuniete;
