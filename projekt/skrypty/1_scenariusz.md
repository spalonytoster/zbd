# Agencja ubezpieczeniowa
-------------------------------------------------
### Założenia

Celem działalności jest świadczenie ubezpieczeń komunikacyjnych dla klientow na terenie Polski. Agencja obsługuje zarówno osoby fizyczne jak i firmy. Współpracuje z wieloma agentami, kancelariami prawniczymi czy spółkami świadczącymi usługi takie jak rzeczoznawstwo oraz posiada własny oddział Contact Center.
