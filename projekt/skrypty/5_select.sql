SELECT
    klienci.klie_id AS id,
    dane_osobowe.daos_imie AS imie,
    dane_osobowe.daos_nazwisko AS nazwisko,
    MIN(polisy.poli_data_zawarcia) AS od_kiedy_klient
FROM
    klienci
    INNER JOIN polisy ON klienci.klie_id = polisy.klienci_klie_id
    INNER JOIN oferty ON oferty.ofer_id = polisy.oferty_ofer_id
    INNER JOIN dane_osobowe ON dane_osobowe.daos_id = klienci.klie_dane_id
WHERE
    klienci.klie_typ = (
        SELECT
            pozycje_slownika.posl_wartosc
        FROM
            slowniki
            INNER JOIN pozycje_slownika ON slowniki.slow_id = pozycje_slownika.slowniki_slow_id
        WHERE
            slowniki.slow_kod = 'TYPY_KLIENTOW'
            AND   pozycje_slownika.posl_kod = 'OSOBA_FIZYCZNA'
    )
    AND   oferty.ofer_data_konc_ochrony > SYSDATE
GROUP BY
    klienci.klie_id,
    dane_osobowe.daos_imie,
    dane_osobowe.daos_nazwisko,
    oferty.ofer_data_konc_ochrony
ORDER BY
    od_kiedy_klient;


SELECT
    jednostki_statystyczne.jsta_id AS id_agenta,
    jednostki_statystyczne.jsta_nazwa AS nazwa_agenta,
    SUM(oferty.ofer_kwota) AS kwota_spolisowana
FROM
    jednostki_statystyczne
    INNER JOIN oferty ON jednostki_statystyczne.jsta_id = oferty.jednostki_statystyczne_jsta_id
    INNER JOIN polisy ON oferty.ofer_id = polisy.oferty_ofer_id
WHERE
    polisy.poli_data_zawarcia > trunc(SYSDATE,'YEAR')
    AND   jednostki_statystyczne.jsta_typ = (
        SELECT
            pozycje_slownika.posl_wartosc
        FROM
            slowniki
            INNER JOIN pozycje_slownika ON slowniki.slow_id = pozycje_slownika.slowniki_slow_id
        WHERE
            pozycje_slownika.posl_kod = 'AGENT'
            AND   slowniki.slow_kod = 'TYPY_JEDNOSTEK'
    )
GROUP BY
    jednostki_statystyczne.jsta_id,
    jednostki_statystyczne.jsta_nazwa
ORDER BY
    kwota_spolisowana DESC;
