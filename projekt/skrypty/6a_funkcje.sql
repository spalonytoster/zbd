FUNCTION DAJ_STRUKTURE_SPRZEDAZY (p_jsta_id IN jednostki_statystyczne.jsta_id%TYPE)
RETURN VARCHAR2
IS
  v_result     VARCHAR2(200) := '';
  v_jsta_id    jednostki_statystyczne.jsta_id%TYPE := p_jsta_id;
  v_jsta_nazwa jednostki_statystyczne.jsta_nazwa%TYPE;
BEGIN
    WHILE v_jsta_id IS NOT NULL LOOP
        SELECT jsta_nazwa
        INTO   v_jsta_nazwa
        FROM   jednostki_statystyczne
        WHERE  jsta_id = v_jsta_id;

        v_result := v_result
                    || v_jsta_nazwa
                    || ' << ';

        SELECT jsta_jsta_id
        INTO   v_jsta_id
        FROM   jednostki_statystyczne
        WHERE  jsta_id = v_jsta_id;
    END LOOP;

    RETURN v_result;
END;
FUNCTION CZY_KLIENT_VIP(p_klient_id IN klienci.klie_id%TYPE)
RETURN CHAR
IS
  v_vip klienci.klie_czy_vip%TYPE;
BEGIN
    SELECT klie_czy_vip
    INTO   v_vip
    FROM   klienci
    WHERE  klie_id = p_klient_id;

    RETURN v_vip;
EXCEPTION
  WHEN no_data_found THEN
             RETURN 'B'; WHEN OTHERS THEN
             RETURN 'B';
END;
FUNCTION DAJ_POZYCJE_SLOWNIKA(p_kod_slownika IN slowniki.slow_kod%TYPE,
                              p_kod_pozycji  IN pozycje_slownika.posl_kod%TYPE)
RETURN pozycje_slownika.posl_wartosc%TYPE
IS
  v_wartosc pozycje_slownika.posl_wartosc%TYPE;
BEGIN
    SELECT posl_wartosc
    INTO   v_wartosc
    FROM   pozycje_slownika
    WHERE  posl_kod = p_kod_pozycji
           AND slowniki_slow_id = (SELECT slow_id
                                   FROM   slowniki
                                   WHERE  slow_kod = p_kod_slownika);

    RETURN v_wartosc;
END;

FUNCTION DODAJ_KLIENTA_OSOBA(p_wofe IN wlasciciele_ofert%ROWTYPE)
RETURN klienci.klie_dane_id%TYPE
IS
  v_klie_dane_id klienci.klie_dane_id%TYPE := dane_osobowe_daos_id_seq.NEXTVAL;
BEGIN
    INSERT INTO dane_osobowe
    VALUES      (v_klie_dane_id,
                 p_wofe.wofe_imie,
                 p_wofe.wofe_nazwisko,
                 p_wofe.wofe_pesel,
                 TO_DATE(SUBSTR('94053004838', 0, 6), 'rrmmdd'));

    DODAJ_ADRES_OSOBA(p_wofe, v_klie_dane_id);

    RETURN v_klie_dane_id;
END;
FUNCTION DODAJ_KLIENTA_FIRMA(p_wofe IN wlasciciele_ofert%ROWTYPE)
RETURN klienci.klie_dane_id%TYPE
IS
  v_klie_dane_id klienci.klie_dane_id%TYPE := dane_firmy_dafi_id_seq.NEXTVAL;
BEGIN
    INSERT INTO dane_firmy
    VALUES      (v_klie_dane_id,
                 p_wofe.wofe_nazwa,
                 p_wofe.wofe_nip);

    DODAJ_ADRES_FIRMA(p_wofe, v_klie_dane_id);

    RETURN v_klie_dane_id;
END;
FUNCTION DAJ_ADRES_KLIENTA(p_klie_id IN klienci.klie_id%TYPE)
RETURN VARCHAR2
IS
  r_adres   adresy%ROWTYPE;
  v_adre_id adresy.adre_id%TYPE;
  v_wynik   VARCHAR2(100);
  r_klie    klienci%ROWTYPE;
BEGIN
    SELECT *
    INTO   r_klie
    FROM   klienci
    WHERE  klie_id = p_klie_id;

    IF r_klie.klie_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'OSOBA_FIZYCZNA') THEN
      SELECT adresy_adre_id
      INTO   v_adre_id
      FROM   daos_adre
      WHERE  dane_osobowe_daos_id = r_klie.klie_dane_id;
    ELSIF r_klie.klie_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'KONTRAHENT') THEN
      SELECT daad_adre_id
      INTO   v_adre_id
      FROM   dafi_adre
      WHERE  daad_dafi_id = r_klie.klie_dane_id;
    END IF;

    SELECT *
    INTO   r_adres
    FROM   adresy
    WHERE  adre_id = v_adre_id;

    v_wynik := r_adres.adre_ulica
               || ' '
               || r_adres.adre_nr_budynku
               || '/'
               || r_adres.adre_nr_lokalu
               || ', '
               || r_adres.adre_kod_pocztowy
               || ' '
               || r_adres.adre_miasto;

    RETURN v_wynik;
END;
