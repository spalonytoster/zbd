PROCEDURE DODAJ_ADRES_OSOBA(p_wofe         IN wlasciciele_ofert%ROWTYPE,
                            p_klie_dane_id IN klienci.klie_dane_id%TYPE)
IS
  v_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
BEGIN
    INSERT INTO adresy
    VALUES      (v_adre_id,
                 p_wofe.wofe_ulica,
                 p_wofe.wofe_nr_budynku,
                 p_wofe.wofe_nr_lokalu,
                 p_wofe.wofe_miasto,
                 p_wofe.wofe_kod_pocztowy);

    INSERT INTO daos_adre
    VALUES      (p_klie_dane_id,
                 v_adre_id);
END;
PROCEDURE DODAJ_ADRES_FIRMA(p_wofe         IN wlasciciele_ofert%ROWTYPE,
                            p_klie_dane_id IN klienci.klie_dane_id%TYPE)
IS
  v_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
BEGIN
    INSERT INTO adresy
    VALUES      (v_adre_id,
                 p_wofe.wofe_ulica,
                 p_wofe.wofe_nr_budynku,
                 p_wofe.wofe_nr_lokalu,
                 p_wofe.wofe_miasto,
                 p_wofe.wofe_kod_pocztowy);

    INSERT INTO dafi_adre
    VALUES      (p_klie_dane_id,
                 v_adre_id);
END;

PROCEDURE POLISUJ_OFERTE(p_ofer_id IN oferty.ofer_id%TYPE)
IS
  v_klie_id klienci.klie_id%TYPE;
  r_wofe    wlasciciele_ofert%ROWTYPE;
  v_wofe_id oferty.wlasciciele_ofert_wofe_id%TYPE;
  v_dane_id klienci.klie_dane_id%TYPE;
BEGIN
    SELECT wlasciciele_ofert_wofe_id
    INTO   v_wofe_id
    FROM   oferty
    WHERE  ofer_id = p_ofer_id;

    SELECT *
    INTO   r_wofe
    FROM   wlasciciele_ofert
    WHERE  wofe_id = v_wofe_id;

    IF r_wofe.wofe_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'OSOBA_FIZYCZNA') THEN
      v_dane_id := DODAJ_KLIENTA_OSOBA(r_wofe);
    ELSIF r_wofe.wofe_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'KONTRAHENT') THEN
      v_dane_id := DODAJ_KLIENTA_FIRMA(r_wofe);
    END IF;

    v_klie_id := klienci_klie_id_seq.NEXTVAL;

    INSERT INTO klienci
    VALUES      (v_klie_id,
                 'N',
                 r_wofe.wofe_typ,
                 v_dane_id);

    INSERT INTO polisy
    VALUES      (NULL,
                 SYSDATE,
                 p_ofer_id,
                 v_klie_id);

    COMMIT;
END;
PROCEDURE DODAJ_DANE_SPRAWCY(p_id             IN dane_osobowe.daos_id%TYPE,
                             p_imie           IN dane_osobowe.daos_imie%TYPE,
                             p_nazwisko       IN dane_osobowe.daos_nazwisko%TYPE,
                             p_pesel          IN dane_osobowe.daos_pesel%TYPE,
                             p_data_urodzenia IN dane_osobowe.daos_data_urodzenia%TYPE)
IS
BEGIN
    INSERT INTO dane_osobowe
    VALUES      (p_id,
                 p_imie,
                 p_nazwisko,
                 p_pesel,
                 p_data_urodzenia);
END;
PROCEDURE DODAJ_ADRES_SPRAWCY(p_id           IN adresy.adre_id%TYPE,
                              p_ulica        IN adresy.adre_ulica%TYPE,
                              p_nr_budynku   IN adresy.adre_nr_budynku%TYPE,
                              p_nr_lokalu    IN adresy.adre_nr_lokalu%TYPE,
                              p_miasto       IN adresy.adre_miasto%TYPE,
                              p_kod_pocztowy IN adresy.adre_kod_pocztowy%TYPE)
IS
BEGIN
    INSERT INTO adresy
    VALUES      (p_id,
                 p_ulica,
                 p_nr_budynku,
                 p_nr_lokalu,
                 p_miasto,
                 p_kod_pocztowy);
END;

-- ponizsza procedura zawiera blad, nie udalo mi sie naprawic

--  PROCEDURE ZALOZ_ROSZCZENIE(p_klie_id        IN klienci.klie_id%TYPE,
--                             p_szko_opis      IN szkody.szko_opis%TYPE,
--                             p_rosz_kwota     IN roszczenia.rosz_kwota%TYPE,
--                             p_rosz_opis      roszczenia.rosz_opis%TYPE,
--                             p_imie           IN dane_osobowe.daos_imie%TYPE,
--                             p_nazwisko       IN dane_osobowe.daos_nazwisko%TYPE,
--                             p_pesel          IN dane_osobowe.daos_pesel%TYPE,
--                             p_data_urodzenia IN dane_osobowe.daos_data_urodzenia%TYPE,
--                             p_ulica          IN adresy.adre_ulica%TYPE,
--                             p_nr_budynku     IN adresy.adre_nr_budynku%TYPE,
--                             p_nr_lokalu      IN adresy.adre_nr_lokalu%TYPE,
--                             p_miasto         IN adresy.adre_miasto%TYPE,
--                             p_kod_pocztowy   IN adresy.adre_kod_pocztowy%TYPE)
--  IS
--    v_rosz_id         roszczenia.rosz_id%TYPE := roszczenia_rosz_id_seq.NEXTVAL;
--    v_klient_daos_id  dane_osobowe.daos_id%TYPE;
--    v_sprawca_daos_id dane_osobowe.daos_id%TYPE := dane_osobowe_daos_id_seq.NEXTVAL;
--    v_sprawca_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
--  BEGIN
--      SELECT klie_dane_id
--      INTO   v_klient_daos_id
--      FROM   klienci
--      WHERE  klie_id = p_klie_id;
--
--      INSERT INTO roszczenia
--      VALUES      (v_rosz_id,
--                   p_rosz_kwota,
--                   DAJ_POZYCJE_SLOWNIKA('STATUSY_ROSZCZEN', 'ZAREJESTROWANO'),
--                   p_rosz_opis);
--
--      INSERT INTO szkody
--      VALUES      (NULL,
--                   p_szko_opis,
--                   v_rosz_id);
--
--      INSERT INTO strony_postepowania
--      VALUES      (NULL,
--                   v_klient_daos_id,
--                   v_rosz_id,
--                   DAJ_POZYCJE_SLOWNIKA('TYPY_STRON_POSTEPOWANIA', 'POSZKODOWANY'));
--
--      DODAJ_DANE_SPRAWCY(v_sprawca_daos_id, p_imie, p_nazwisko, p_pesel, p_data_urodzenia);
--
--      DODAJ_ADRES_SPRAWCY(v_sprawca_adre_id, p_ulica, p_nr_budynku, p_nr_lokalu, p_miasto, p_kod_pocztowy);
--
--      INSERT INTO daos_adre
--      VALUES      (v_sprawca_daos_id,
--                   v_sprawca_adre_id);
--
--      INSERT INTO strony_postepowania
--      VALUES      (NULL,
--                   p_daos_sprawca.daos_id,
--                   v_rosz_id,
--                   DAJ_POZYCJE_SLOWNIKA('TYPY_STRON_POSTEPOWANIA', 'SPRAWCA'));
--
--      COMMIT;
--  END;
