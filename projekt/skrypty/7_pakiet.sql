CREATE OR replace PACKAGE zasoby
IS
  -- FUNKCJE
  FUNCTION DAJ_STRUKTURE_SPRZEDAZY (
    p_jsta_id IN jednostki_statystyczne.jsta_id%TYPE )
  RETURN VARCHAR2;
  FUNCTION CZY_KLIENT_VIP(
    p_klient_id IN klienci.klie_id%TYPE)
  RETURN CHAR;
  FUNCTION DAJ_POZYCJE_SLOWNIKA(
    p_kod_slownika IN slowniki.slow_kod%TYPE,
    p_kod_pozycji  IN pozycje_slownika.posl_kod%TYPE)
  RETURN pozycje_slownika.posl_wartosc%TYPE;
  FUNCTION DAJ_ADRES_KLIENTA(
    p_klie_id IN klienci.klie_id%TYPE)
  RETURN VARCHAR2;
  -- PROCEDURY
  PROCEDURE polisuj_oferte(
    p_ofer_id IN oferty.ofer_id%TYPE);

--  PROCEDURE ZALOZ_ROSZCZENIE(p_klie_id        IN klienci.klie_id%TYPE,
--                             p_szko_opis      IN szkody.szko_opis%TYPE,
--                             p_rosz_kwota     IN roszczenia.rosz_kwota%TYPE,
--                             p_rosz_opis      roszczenia.rosz_opis%TYPE,
--                             p_imie           IN dane_osobowe.daos_imie%TYPE,
--                             p_nazwisko       IN dane_osobowe.daos_nazwisko%TYPE,
--                             p_pesel          IN dane_osobowe.daos_pesel%TYPE,
--                             p_data_urodzenia IN dane_osobowe.daos_data_urodzenia%TYPE,
--                             p_ulica          IN adresy.adre_ulica%TYPE,
--                             p_nr_budynku     IN adresy.adre_nr_budynku%TYPE,
--                             p_nr_lokalu      IN adresy.adre_nr_lokalu%TYPE,
--                             p_miasto         IN adresy.adre_miasto%TYPE,
--                             p_kod_pocztowy   IN adresy.adre_kod_pocztowy%TYPE);
END zasoby;
/

CREATE OR replace PACKAGE BODY zasoby
IS
  /*
   *  Funkcja zwraca lancuch znakow prezentujacy siec struktury sprzedazy od wybranego elementu do korzenia (centrala)
   */
  FUNCTION DAJ_STRUKTURE_SPRZEDAZY (p_jsta_id IN jednostki_statystyczne.jsta_id%TYPE)
  RETURN VARCHAR2
  IS
    v_result     VARCHAR2(200) := '';
    v_jsta_id    jednostki_statystyczne.jsta_id%TYPE := p_jsta_id;
    v_jsta_nazwa jednostki_statystyczne.jsta_nazwa%TYPE;
  BEGIN
      WHILE v_jsta_id IS NOT NULL LOOP
          SELECT jsta_nazwa
          INTO   v_jsta_nazwa
          FROM   jednostki_statystyczne
          WHERE  jsta_id = v_jsta_id;

          v_result := v_result
                      || v_jsta_nazwa
                      || ' << ';

          SELECT jsta_jsta_id
          INTO   v_jsta_id
          FROM   jednostki_statystyczne
          WHERE  jsta_id = v_jsta_id;
      END LOOP;

      RETURN v_result;
  END;
  FUNCTION CZY_KLIENT_VIP(p_klient_id IN klienci.klie_id%TYPE)
  RETURN CHAR
  IS
    v_vip klienci.klie_czy_vip%TYPE;
  BEGIN
      SELECT klie_czy_vip
      INTO   v_vip
      FROM   klienci
      WHERE  klie_id = p_klient_id;

      RETURN v_vip;
  EXCEPTION
    WHEN no_data_found THEN
               RETURN 'B'; WHEN OTHERS THEN
               RETURN 'B';
  END;
  FUNCTION DAJ_POZYCJE_SLOWNIKA(p_kod_slownika IN slowniki.slow_kod%TYPE,
                                p_kod_pozycji  IN pozycje_slownika.posl_kod%TYPE)
  RETURN pozycje_slownika.posl_wartosc%TYPE
  IS
    v_wartosc pozycje_slownika.posl_wartosc%TYPE;
  BEGIN
      SELECT posl_wartosc
      INTO   v_wartosc
      FROM   pozycje_slownika
      WHERE  posl_kod = p_kod_pozycji
             AND slowniki_slow_id = (SELECT slow_id
                                     FROM   slowniki
                                     WHERE  slow_kod = p_kod_slownika);

      RETURN v_wartosc;
  END;
  PROCEDURE DODAJ_ADRES_OSOBA(p_wofe         IN wlasciciele_ofert%ROWTYPE,
                              p_klie_dane_id IN klienci.klie_dane_id%TYPE)
  IS
    v_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
  BEGIN
      INSERT INTO adresy
      VALUES      (v_adre_id,
                   p_wofe.wofe_ulica,
                   p_wofe.wofe_nr_budynku,
                   p_wofe.wofe_nr_lokalu,
                   p_wofe.wofe_miasto,
                   p_wofe.wofe_kod_pocztowy);

      INSERT INTO daos_adre
      VALUES      (p_klie_dane_id,
                   v_adre_id);
  END;
  PROCEDURE DODAJ_ADRES_FIRMA(p_wofe         IN wlasciciele_ofert%ROWTYPE,
                              p_klie_dane_id IN klienci.klie_dane_id%TYPE)
  IS
    v_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
  BEGIN
      INSERT INTO adresy
      VALUES      (v_adre_id,
                   p_wofe.wofe_ulica,
                   p_wofe.wofe_nr_budynku,
                   p_wofe.wofe_nr_lokalu,
                   p_wofe.wofe_miasto,
                   p_wofe.wofe_kod_pocztowy);

      INSERT INTO dafi_adre
      VALUES      (p_klie_dane_id,
                   v_adre_id);
  END;
  FUNCTION DODAJ_KLIENTA_OSOBA(p_wofe IN wlasciciele_ofert%ROWTYPE)
  RETURN klienci.klie_dane_id%TYPE
  IS
    v_klie_dane_id klienci.klie_dane_id%TYPE := dane_osobowe_daos_id_seq.NEXTVAL;
  BEGIN
      INSERT INTO dane_osobowe
      VALUES      (v_klie_dane_id,
                   p_wofe.wofe_imie,
                   p_wofe.wofe_nazwisko,
                   p_wofe.wofe_pesel,
                   TO_DATE(SUBSTR('94053004838', 0, 6), 'rrmmdd'));

      DODAJ_ADRES_OSOBA(p_wofe, v_klie_dane_id);

      RETURN v_klie_dane_id;
  END;
  FUNCTION DODAJ_KLIENTA_FIRMA(p_wofe IN wlasciciele_ofert%ROWTYPE)
  RETURN klienci.klie_dane_id%TYPE
  IS
    v_klie_dane_id klienci.klie_dane_id%TYPE := dane_firmy_dafi_id_seq.NEXTVAL;
  BEGIN
      INSERT INTO dane_firmy
      VALUES      (v_klie_dane_id,
                   p_wofe.wofe_nazwa,
                   p_wofe.wofe_nip);

      DODAJ_ADRES_FIRMA(p_wofe, v_klie_dane_id);

      RETURN v_klie_dane_id;
  END;
  FUNCTION DAJ_ADRES_KLIENTA(p_klie_id IN klienci.klie_id%TYPE)
  RETURN VARCHAR2
  IS
    r_adres   adresy%ROWTYPE;
    v_adre_id adresy.adre_id%TYPE;
    v_wynik   VARCHAR2(100);
    r_klie    klienci%ROWTYPE;
  BEGIN
      SELECT *
      INTO   r_klie
      FROM   klienci
      WHERE  klie_id = p_klie_id;

      IF r_klie.klie_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'OSOBA_FIZYCZNA') THEN
        SELECT adresy_adre_id
        INTO   v_adre_id
        FROM   daos_adre
        WHERE  dane_osobowe_daos_id = r_klie.klie_dane_id;
      ELSIF r_klie.klie_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'KONTRAHENT') THEN
        SELECT daad_adre_id
        INTO   v_adre_id
        FROM   dafi_adre
        WHERE  daad_dafi_id = r_klie.klie_dane_id;
      END IF;

      SELECT *
      INTO   r_adres
      FROM   adresy
      WHERE  adre_id = v_adre_id;

      v_wynik := r_adres.adre_ulica
                 || ' '
                 || r_adres.adre_nr_budynku
                 || '/'
                 || r_adres.adre_nr_lokalu
                 || ', '
                 || r_adres.adre_kod_pocztowy
                 || ' '
                 || r_adres.adre_miasto;

      RETURN v_wynik;
  END;
  PROCEDURE POLISUJ_OFERTE(p_ofer_id IN oferty.ofer_id%TYPE)
  IS
    v_klie_id klienci.klie_id%TYPE;
    r_wofe    wlasciciele_ofert%ROWTYPE;
    v_wofe_id oferty.wlasciciele_ofert_wofe_id%TYPE;
    v_dane_id klienci.klie_dane_id%TYPE;
  BEGIN
      SELECT wlasciciele_ofert_wofe_id
      INTO   v_wofe_id
      FROM   oferty
      WHERE  ofer_id = p_ofer_id;

      SELECT *
      INTO   r_wofe
      FROM   wlasciciele_ofert
      WHERE  wofe_id = v_wofe_id;

      IF r_wofe.wofe_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'OSOBA_FIZYCZNA') THEN
        v_dane_id := DODAJ_KLIENTA_OSOBA(r_wofe);
      ELSIF r_wofe.wofe_typ = zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'KONTRAHENT') THEN
        v_dane_id := DODAJ_KLIENTA_FIRMA(r_wofe);
      END IF;

      v_klie_id := klienci_klie_id_seq.NEXTVAL;

      INSERT INTO klienci
      VALUES      (v_klie_id,
                   'N',
                   r_wofe.wofe_typ,
                   v_dane_id);

      INSERT INTO polisy
      VALUES      (NULL,
                   SYSDATE,
                   p_ofer_id,
                   v_klie_id);

      COMMIT;
  END;
  PROCEDURE DODAJ_DANE_SPRAWCY(p_id             IN dane_osobowe.daos_id%TYPE,
                               p_imie           IN dane_osobowe.daos_imie%TYPE,
                               p_nazwisko       IN dane_osobowe.daos_nazwisko%TYPE,
                               p_pesel          IN dane_osobowe.daos_pesel%TYPE,
                               p_data_urodzenia IN dane_osobowe.daos_data_urodzenia%TYPE)
  IS
  BEGIN
      INSERT INTO dane_osobowe
      VALUES      (p_id,
                   p_imie,
                   p_nazwisko,
                   p_pesel,
                   p_data_urodzenia);
  END;
  PROCEDURE DODAJ_ADRES_SPRAWCY(p_id           IN adresy.adre_id%TYPE,
                                p_ulica        IN adresy.adre_ulica%TYPE,
                                p_nr_budynku   IN adresy.adre_nr_budynku%TYPE,
                                p_nr_lokalu    IN adresy.adre_nr_lokalu%TYPE,
                                p_miasto       IN adresy.adre_miasto%TYPE,
                                p_kod_pocztowy IN adresy.adre_kod_pocztowy%TYPE)
  IS
  BEGIN
      INSERT INTO adresy
      VALUES      (p_id,
                   p_ulica,
                   p_nr_budynku,
                   p_nr_lokalu,
                   p_miasto,
                   p_kod_pocztowy);
  END;

  -- ponizsza procedura zawiera blad, nie udalo mi sie naprawic

--  PROCEDURE ZALOZ_ROSZCZENIE(p_klie_id        IN klienci.klie_id%TYPE,
--                             p_szko_opis      IN szkody.szko_opis%TYPE,
--                             p_rosz_kwota     IN roszczenia.rosz_kwota%TYPE,
--                             p_rosz_opis      roszczenia.rosz_opis%TYPE,
--                             p_imie           IN dane_osobowe.daos_imie%TYPE,
--                             p_nazwisko       IN dane_osobowe.daos_nazwisko%TYPE,
--                             p_pesel          IN dane_osobowe.daos_pesel%TYPE,
--                             p_data_urodzenia IN dane_osobowe.daos_data_urodzenia%TYPE,
--                             p_ulica          IN adresy.adre_ulica%TYPE,
--                             p_nr_budynku     IN adresy.adre_nr_budynku%TYPE,
--                             p_nr_lokalu      IN adresy.adre_nr_lokalu%TYPE,
--                             p_miasto         IN adresy.adre_miasto%TYPE,
--                             p_kod_pocztowy   IN adresy.adre_kod_pocztowy%TYPE)
--  IS
--    v_rosz_id         roszczenia.rosz_id%TYPE := roszczenia_rosz_id_seq.NEXTVAL;
--    v_klient_daos_id  dane_osobowe.daos_id%TYPE;
--    v_sprawca_daos_id dane_osobowe.daos_id%TYPE := dane_osobowe_daos_id_seq.NEXTVAL;
--    v_sprawca_adre_id adresy.adre_id%TYPE := adresy_adre_id_seq.NEXTVAL;
--  BEGIN
--      SELECT klie_dane_id
--      INTO   v_klient_daos_id
--      FROM   klienci
--      WHERE  klie_id = p_klie_id;
--
--      INSERT INTO roszczenia
--      VALUES      (v_rosz_id,
--                   p_rosz_kwota,
--                   DAJ_POZYCJE_SLOWNIKA('STATUSY_ROSZCZEN', 'ZAREJESTROWANO'),
--                   p_rosz_opis);
--
--      INSERT INTO szkody
--      VALUES      (NULL,
--                   p_szko_opis,
--                   v_rosz_id);
--
--      INSERT INTO strony_postepowania
--      VALUES      (NULL,
--                   v_klient_daos_id,
--                   v_rosz_id,
--                   DAJ_POZYCJE_SLOWNIKA('TYPY_STRON_POSTEPOWANIA', 'POSZKODOWANY'));
--
--      DODAJ_DANE_SPRAWCY(v_sprawca_daos_id, p_imie, p_nazwisko, p_pesel, p_data_urodzenia);
--
--      DODAJ_ADRES_SPRAWCY(v_sprawca_adre_id, p_ulica, p_nr_budynku, p_nr_lokalu, p_miasto, p_kod_pocztowy);
--
--      INSERT INTO daos_adre
--      VALUES      (v_sprawca_daos_id, 
--                   v_sprawca_adre_id);
--
--      INSERT INTO strony_postepowania
--      VALUES      (NULL,
--                   p_daos_sprawca.daos_id,
--                   v_rosz_id,
--                   DAJ_POZYCJE_SLOWNIKA('TYPY_STRON_POSTEPOWANIA', 'SPRAWCA'));
--
--      COMMIT;
--  END;
END zasoby;
/

SET serveroutput ON;
BEGIN
    NULL;

        dbms_output.PUT_LINE(zasoby.DAJ_STRUKTURE_SPRZEDAZY(10));
        dbms_output.PUT_LINE(zasoby.CZY_KLIENT_VIP(10));
        dbms_output.PUT_LINE(zasoby.DAJ_POZYCJE_SLOWNIKA('TYPY_KLIENTOW', 'OSOBA_FIZYCZNA'));
        zasoby.POLISUJ_OFERTE(1);
        zasoby.POLISUJ_OFERTE(2);
        zasoby.POLISUJ_OFERTE(3);
        zasoby.POLISUJ_OFERTE(4);
        zasoby.POLISUJ_OFERTE(5);
        zasoby.POLISUJ_OFERTE(6);
        zasoby.POLISUJ_OFERTE(7);
        zasoby.POLISUJ_OFERTE(8);
        zasoby.POLISUJ_OFERTE(9);
        zasoby.POLISUJ_OFERTE(10);
        dbms_output.PUT_LINE(zasoby.DAJ_ADRES_KLIENTA(8));
--    zasoby.ZALOZ_ROSZCZENIE(1, 'Wgnieciony zderzak tylny', 1000, 'Roszczenie dot. szkody', 'Ambroży', 'Walenciak', '11111112110', sysdate, 'KOSMICZNA', '12', '1', 'GDAŃSK', '11-111');
END;
/
